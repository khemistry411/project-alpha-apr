from django.forms import ModelForm  # djangodocs
from projects.models import Project

# receipts.forms.py


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "owner",
            "description",
        ]
