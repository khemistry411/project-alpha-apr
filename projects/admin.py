from django.contrib import admin
from projects.models import Project


# Register your models here. #connects to admin site and rendered in html,
# proper grammer is important
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
        "id",
    )
