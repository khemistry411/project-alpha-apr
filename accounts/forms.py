from django import forms


class loginform(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


# signupform - then import to views
# username -150
# password w/passwordinput -150(forms.)
# password_confirmation w/passwordinput -150 (forms.)


class signupform(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )
