from django.shortcuts import render, redirect
from accounts.forms import loginform, signupform
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = loginform(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = loginform()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


# create signup function view import from forms.py


# echo loginview
def user_signup(request):
    if request.method == "POST":
        form = signupform(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                # use create_user method to create user then use login function

                login(request, user)
                return redirect("list_projects")
            # terminal error - after signup does not redirect?
            else:
                form.add_error("password", "Passwords do not match,try again")

    else:
        form = signupform()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)


# create registration and html
