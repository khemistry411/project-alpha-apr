from django.db import models
from django.contrib.auth.models import User
from projects.models import Project


# Create your models here.
# projects.Project mode? import?


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", null=True, on_delete=models.CASCADE
    )


# class Task(models.Model):
# name = models.CharField(max_length=200)
# start_date = models.DateTimeField(auto_now_add=False)
# due_date = models.DateTimeField(auto_now_add=False)
# is_completed = models.BooleanField(null=False) ?? default is?
#  project = models.ForeignKey(Project, related_name="tasks",
#                             on_delete=models.CASCADE,)
#  assignee = models.ForeignKey(User, related_name="tasks",
# n_delete=models.CASCADE, null=True)
