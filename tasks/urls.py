from django.urls import path
from tasks.views import create_new_task, show_tasks


urlpatterns = [
    path("create/", create_new_task, name="create_task"),
    path("mine/", show_tasks, name="show_my_tasks"),
]
