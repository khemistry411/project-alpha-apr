from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from tasks.forms import TaskForm
from tasks.models import Task


# Create your views here.echo projects/views.py create_projects
@login_required
def create_new_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")  # redirect to home
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/createtwo.html", context)


@login_required
def show_tasks(request):
    my_tasks = Task.objects.all()
    context = {"my_tasks": my_tasks}

    return render(request, "tasks/task_list.html", context)
